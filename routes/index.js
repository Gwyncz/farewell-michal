var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET reward page. */
router.get('/', function (req, res, next) {
    var reward = '';
    var shuffled = '';

    fs.readFile('shuffled.txt', (err, buf) => {
        if (err) {
            res.status(500).send('Something broke!');
            next(err);
        }
        shuffled = buf.toString();
        reward = reverseShuffle(shuffled);
        res.render('index', {reward: reward});
    });
});

//just to see, how shuffle of the original has been made
router.get('/shuffle', function (req, res, next) {
    var reward = '';
    var sf = fs.createWriteStream('shuffled.txt', {
        flags: 'a' // 'a' means appending (old data will be preserved)
    });

    fs.readFile('source.txt', (err, buf) => {
        reward = buf.toString();
        var lines = reward.split(/\r\n/);
        var shuffled = shuffle(lines);
        for(let i = 0; i < shuffled.length; i++) {
            sf.write(shuffled[i]+'\r\n');
        }
        sf.end();
        next();
    });
});

//just to see, how shuffle of the original has been made
function shuffle(reward) {
    for (let i = reward.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        console.log('shuffleSteps['+i+'] = '+j+';');
        [reward[i], reward[j]] = [reward[j], reward[i]];
    }
    return reward;
}

//finish it and you'll get your reward
function reverseShuffle(shuffled) {
    var lines = shuffled.split('\r\n');

    //shuffle process
    var shuffleSteps = [];
    shuffleSteps[22] = 9;
    shuffleSteps[21] = 19;
    shuffleSteps[20] = 11;
    shuffleSteps[19] = 6;
    shuffleSteps[18] = 11;
    shuffleSteps[17] = 10;
    shuffleSteps[16] = 2;
    shuffleSteps[15] = 14;
    shuffleSteps[14] = 0;
    shuffleSteps[13] = 1;
    shuffleSteps[12] = 10;
    shuffleSteps[11] = 6;
    shuffleSteps[10] = 5;
    shuffleSteps[9] = 0;
    shuffleSteps[8] = 5;
    shuffleSteps[7] = 5;
    shuffleSteps[6] = 0;
    shuffleSteps[5] = 3;
    shuffleSteps[4] = 2;
    shuffleSteps[3] = 3;
    shuffleSteps[2] = 0;
    shuffleSteps[1] = 0;

    /**
     * HERE WRITE THE SHUFFLE REVERSE ALGORITHM
     */

    return '';
}

module.exports = router;
